import java.util.List;
import java.util.ArrayList;
import utfpr.ct.dainf.if62c.pratica.Time;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.JogadorComparator;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author joao lucas
 */
public class Pratica62
{
  public static void main(String[] args)
  {
    Time time1 = new Time();
    Time time2 = new Time();

    time1.addJogador("Goleiro", new Jogador(1, "Tafarel"));
    time1.addJogador("Meio", new Jogador(8, "Pato"));
    time1.addJogador("Atacante", new Jogador(11, "Dibraldinho"));

    time2.addJogador("Goleiro", new Jogador(1, "Ceni"));
    time2.addJogador("Meio", new Jogador(7, "Pirlo"));
    time2.addJogador("Atacante", new Jogador(9, "Kaka"));

    //time1.compara(time2);
    
    List<Jogador> jogadores_1 = new ArrayList<Jogador>();
    List<Jogador> jogadores_2 = new ArrayList<Jogador>();
    
    jogadores_1 = time1.ordena(new JogadorComparator(true, true, false));
    jogadores_2 = time2.ordena(new JogadorComparator(true, true, false));
      
    for(int i = 0 ; i < 3 ; i++)
      System.out.println(jogadores_1.get(i).nome + " " + jogadores_1.get(i).numero);
    for(int i = 0 ; i < 3 ; i++)
      System.out.println(jogadores_2.get(i).nome + " " + jogadores_2.get(i).numero);
  }
}
